const wordArr = ['jogo', 'estudo', 'serie','filme', 'anime', 'comida', 'palavra', 'esporte', 'letra', 'computador', 'programação', 'desenvolvedor', 'junior', 'pleno', 'senior', 'instrutor', 'facilitador', 'coach', 'quarter', 'kenzie']

    function toRandom(n) {
        return Math.floor(Math.random() * n); 
    }

    let letterArr = [];

    function getRandomLetter() {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";
    let randomLetter = alphabet[Math.floor(Math.random() * alphabet.length)];
    return randomLetter;
    }

// MONTANDO O ARRAY DE LETRAS 10X10 
function buildArr() {
   for (let i = 0; i < 10; i++) {
        letterArr.push([])
        for (let j = 0; j < 10; j++) {
            const letter = getRandomLetter();
            letterArr[i].push(letter);
        }
    }
}

function alocateVertical(chosenWord) {
    let word = chosenWord.split('');
    const wordRow = toRandom(10);
    const wordColumn = toRandom(10 - word.length);
    const wordSize = word.length + wordColumn;
    let count = 0;
    for (let i = wordColumn; i < wordSize; i++) {
        letterArr[wordRow][i] = word[count];
        count++;
    }
}

function cacaPalavras() {
    document.getElementById('divGame').innerHTML = '';
    letterArr = [];
    buildArr();

    let firstWord = wordArr[toRandom(20)]; // define uma palavra aleatória pra cada
    let secondWord = wordArr[toRandom(20)];
    let thirdWord = wordArr[toRandom(20)];

    while (secondWord === firstWord) {   // checa se as palavras são iguais e troca
        secondWord = wordArr[toRandom(20)];
    }

    while (thirdWord === secondWord | thirdWord === firstWord) {
        thirdWord = wordArr[toRandom(20)];
    }

    alocateVertical(firstWord);
    alocateVertical(secondWord);
    alocateVertical(thirdWord);

    const divGame = document.getElementById('divGame');
    
    for (let i = 0; i < 10; i++) {
        const span = document.createElement('span');
        span.id = span + i
        divGame.appendChild(span);
        let newArr = letterArr[i].toString().replace(/,/g,"");

        for (let j = 0; j < 10; j++) {
            span.innerHTML += newArr[j];
        }
    }
}

const buttonGame = document.getElementById('buttonGame');
buttonGame.addEventListener('click', cacaPalavras);